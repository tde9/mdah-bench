# MDAH clients (unscientific) benchmarks

## Results

Clients tested:

- [Official](https://gitlab.com/mangadex-pub/mangadex_at_home)  2.x
- [Mdathome Golang](https://github.com/lflare/mdathome-golang)  1.10.x
- [kmdah](https://github.com/Tristan971/kmdah)                  0.4.x

| Client        | Rqps @ 10 conn    | Rqps @ 100 conn   | Rqps @ 200 conn   |
|:-----------   |:-----------:      |:-----------:      |:-----------:      |
CLIENT_SUMMARY_LINE

## Some notes

It's in gitlab-ci, without any control to make sure every client gets exactly the same CPU/Memory/FS speeds. This is partially why it's not a scientific
comparison.

*kmdah* is not SSL-aware, which makes things easier for it. As it is meant to be used inside a kubernetes cluster, where a reverse proxy is always present. It
then just configures that reverse proxy at runtime with the certificate it receives from the backend.
