#!/usr/bin/env bash

set -euo pipefail

function verify_result() {
  if ! grep -Ei "^Document Length:[ ]+708370 bytes" "$1"; then
    echo "Warning: incorrect document length!"
  fi
}

function bench() {
  local RUNS_BENCH="$1"
  local PARALLEL="$2"

  mkdir -pv "bench/$CLIENT_NAME"
  local TARGET_OUT="bench/$CLIENT_NAME/$3"

  echo "Benching $RUNS_BENCH runs with $PARALLEL connections (output: $TARGET_OUT)"

  if (
    set -x
    ab -n "$RUNS_BENCH" -c "$PARALLEL" "$SCHEME://127.0.0.1:44300/deadbeef/data/8172a46adc798f4f4ace6663322a383e/B18-8ceda4f88ddf0b2474b1017b6a3c822ea60d61e454f7e99e34af2cf2c9037b84.png" | tee "$TARGET_OUT"
  ); then
    verify_result "$TARGET_OUT"
  else
    echo "Failed run! (exit code $?)"
    exit 1
  fi

  echo "Sleeping 3s after bench run..."
  sleep 3
}

echo "-----------------------------------------"
echo "Warming..."
bench "5000" "100" "bench_warm.log"

echo "-----------------------------------------"
bench "3000" "10" "bench_10.log"

echo "-----------------------------------------"
bench "3000" "100" "bench_100.log"

echo "-----------------------------------------"
bench "3000" "200" "bench_200.log"
