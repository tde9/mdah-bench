# MDAH clients (unscientific) benchmarks

## Results

Clients tested:

- [Official](https://gitlab.com/mangadex-pub/mangadex_at_home)  2.x
- [Mdathome Golang](https://github.com/lflare/mdathome-golang)  1.10.x
- [kmdah](https://github.com/Tristan971/kmdah)                  0.4.x

| Client        | Rqps @ 10 conn    | Rqps @ 100 conn   | Rqps @ 200 conn   |
|:-----------   |:-----------:      |:-----------:      |:-----------:      |
| official-2.x | 1859.26 (avg 5.378 ms) | 1761.44 (avg 56.772 ms) | 1781.53 (avg 112.263 ms) |
| mdathome-golang-1.10.x | 1779.38 (avg 5.620 ms) | 1665.93 (avg 60.027 ms) | 1587.94 (avg 125.949 ms) |
| kmdah-0.4.x | 6645.67 (avg 1.505 ms) | 4584.51 (avg 21.813 ms) | 5255.74 (avg 38.054 ms) |

## Some notes

It's in gitlab-ci, without any control to make sure every client gets exactly the same CPU/Memory/FS speeds. This is partially why it's not a scientific
comparison.

*kmdah* is not SSL-aware, which makes things easier for it. As it is meant to be used inside a kubernetes cluster, where a reverse proxy is always present. It
then just configures that reverse proxy at runtime with the certificate it receives from the backend.
